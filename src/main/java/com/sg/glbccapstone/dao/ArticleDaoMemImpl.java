/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.glbccapstone.dao;

import com.sg.glbccapstone.model.Article;
import com.sg.glbccapstone.model.SearchTerm;
import com.sg.glbccapstone.model.Tag;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author Jay Pavlinak
 */
public class ArticleDaoMemImpl implements ArticleDao {
	
	private JdbcTemplate jdbcTemplate;
	private static Map<Integer, Article> articleList;
	private static int articleID;
	
	public ArticleDaoMemImpl(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
		
		/*
		1, 1, true, "Artical on Epiphany", "Best. Beer. Ever.", "Body of article", "11-16-16", ""),
(2, 2, true, "Artical on Substance", "Very hard to get.. but worthing it you can.", "Body of article", "11-16-16", ""),
(3, 2, true, "Artical on Space Gose", "Fantastic on a hot summer day.", "Body of article", "11-16-16", "");
		*/
		
		Article article = new Article();
		article.setArticleName("An Epiphany From the Gods");
		article.setSummary("Best. Beer. Ever");
	}

	@Override
	public Article createArticle(Article article) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public List<Article> getAllArticles() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public List<Article> getAllArticlesByUserName(String username) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public List<Article> getAllArticlesByTagName(String tagName) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public List<Article> getAllArticlesByCategoryId(int categoryId) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public List<Article> getAllArticlesByApproval(boolean approved) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Article getArticleById(int articleId) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public List<Article> getMostRecentApprovedArticle(boolean approved, int limit) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void updateArticle(Article article) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void removeArticle(int articleId) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public List<Article> searchArticles(Map<SearchTerm, String> criteria) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void addTagsToArticle(List<Tag> tagList, Article article) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}
	
}
