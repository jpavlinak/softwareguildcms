function articleSearchAjaxCall(searchCriteria) {
	
	$.ajax({
		method: "POST",
		url: "searchArticle",
		data: JSON.stringify({articleSearchCriteria: searchCriteria}),
		
		headers: {
			"Accept": "application/json",
			"Content-Type": "application/json"
		},
		
		contentType: "application/json; charset=utf-8",
		dataType: "json"
		
	}).success(function (data) {
		fillArticleSearchTable(data, status);
	});
}

function fillArticleSearchTable(searchResults, status) {
	
	applyCSSFormatting($("#searchResultArticleTable"));
	
    var articleSearchResultsTable = $("#searchResultArticlesTableBody");
	
    $.each(searchResults, function (index, article) {
        articleSearchResultsTable.append($("<tr>")
                .append($("<td>").text(article.userName))
                .append($("<td>").append($("<a>").attr({'onClick': 'loadArticleFromSearch(' + article.articleId + ')'}).text(article.articleName)))
                .append($("<td>").text(article.summary))
                );
    });
}

function loadArticleFromSearch(articleId) {
	
    $.ajax({
        type: 'GET',
        url: 'article/' + articleId,
		
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        },
		
        contentType: "application/json; charset=utf-8",
        dataType: "json"
		
    }).success(function (data) {
		
		clearDetailsPanes();
        displaySearchedArticleFromSearch(data);
		
    }).error(function () {
        alert("This didn't work");
    });
}

function displaySearchedArticleFromSearch(article, status) {
	
	applyCSSFormatting($("#searchResultArticleContent"));
	
    var recentArticle = $("#searchResultArticle");
	
    recentArticle.append($("<div.searchResultArticleTitle>").addClass('searchResultArticleTitle').html(article.articleName))
		.append($("<a>").attr({'onClick': 'loadArticlesByCategoryId(' + article.categoryId + ')'}).append($("<div.searchResultArticleCategory>").html("Category: " + article.categoryName)))
		.append($("<div.searchResultArticleUserName>").addClass('searchResultArticleUserName').html(article.userName))
		.append($("<div.searchResultArticlePublicationDate>").addClass('searchResultArticlePublicationDate').html(article.publishDate))
		.append($("<div.searchResultArticleTextBody>").html(article.textBody));
}