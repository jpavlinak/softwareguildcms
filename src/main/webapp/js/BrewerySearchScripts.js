function brewerySearchAjaxCall(searchCriteria) {
	
	$.ajax({
		method: "POST",
		url: "searchBrewery",
		data: JSON.stringify({brewerySearchCriteria: searchCriteria}),
		
		headers: {
			"Accept": "application/json",
			"Content-Type": "application/json"
		},
		
		contentType: "application/json; charset=utf-8",
		dataType: "json"
		
	}).success(function (data) {
		fillBrewerySearchTable(data, status);
	});
}

function fillBrewerySearchTable(searchResults, status) {
	
	applyCSSFormatting($('#searchResultBreweryTable'));
	
	var brewerySearchResultsTable = $("#searchResultBreweryTableBody");
	
	$.each(searchResults, function (index, brewery) {
		brewerySearchResultsTable.append($("<tr>")
				.append($("<td>").append($("<a>").attr({'onClick': 'loadBrewery(' + brewery.breweryId + ')'}).text(brewery.breweryName)))
				.append($("<td>").text(brewery.city + ", " + brewery.state))
				.append($("<td>").text(brewery.summary)));
	});
}

function loadBrewery(breweryId) {
	
    $.ajax({
        type: 'GET',
        url: 'brewery/' + breweryId,
		
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        },
		
        contentType: "application/json; charset=utf-8",
        dataType: "json"
		
    }).success(function (data) {
		
		clearDetailsPanes();
        displaySearchedBrewery(data);
		
    }).error(function () {
        alert("This didn't work");
    });
}

function displaySearchedBrewery(brewery) {
	
	applyCSSFormatting($('#searchResultBreweryContent'));
	
    var searchedBrewery = $("#searchResultBrewery");
	
    searchedBrewery.append($("<div.searchResultBreweryName>").addClass('searchResultBreweryName').html(brewery.breweryName))
			.append($("<div.searchResultBreweryLocation>").addClass('searchResultBreweryLocation').html(brewery.city + ", " + brewery.state))
            .append($("<div.searchResultBreweryTextBody>").addClass('searchResultBreweryTextBody').html(brewery.textBody));
}