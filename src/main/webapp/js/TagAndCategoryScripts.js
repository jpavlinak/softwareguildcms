$(document).on('click', '.tag', function () {
	var string = $(this).data("tagname");
	
	clearStaticPage();
	clearCSSFormatting();
	clearSearchTables();
	clearDetailsPanes();

	loadArticlesByTagName(string);
});

function loadArticlesByTagName(string) {
	
	var tagName = string;
	
	$('#thead').empty();
	$('#thead').append($("<th>").css('display', 'block').text("Tag: " + tagName));
	
	$.ajax({
		type: 'GET',
		url: 'tag/' + tagName,
		
		headers: {
			"Accept": "application/json",
			"Content-Type": "application/json"
		},
		
		contentType: "application/json; charset=utf-8",
		dataType: "json"
		
	}).success(function (articleList, status) {
		
		$("#searchResultArticleTableHead").text("Tag: " + tagName);
		fillArticleSearchTable(articleList);
		
	}).error(function () {
		alert("loadArticlesByTagName didn't work");
	});
}

function loadArticlesByCategoryId(categoryId, status) {
	
	clearStaticPage();
	clearCSSFormatting();
	clearSearchTables();
	clearDetailsPanes();
	
    $.ajax({
        type: 'GET',
        url: 'articles/' + categoryId,
		
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        },
		
        contentType: "application/json; charset=utf-8",
        dataType: "json"
		
    }).success(function (articleList, status) {
		
		$("#searchResultArticleTableHead").text("Category: " + articleList[0].categoryName);
        fillArticleTableByCategory(articleList);
		
    }).error(function () {
        alert("loadArticlesByCacategorytegoryId didn't work");
    });
}

function fillArticleTableByCategory(articleList) {
	
	applyCSSFormatting($('#searchResultArticleTable'));
	
	$("#searchResultArticlesTableBody").empty();
	
	var articleSearchResultsTable = $("#searchResultArticlesTableBody");
	
	$.each(articleList, function (index, article) {
		articleSearchResultsTable.append($("<tr>")
				.append($("<td>").text(article.userName))
				.append($("<td>").append($("<a>").attr({'onClick': 'loadArticleFromSearch(' + article.articleId + ')'}).text(article.articleName)))
				.append($("<td>").text(article.summary))
				);
	});
}