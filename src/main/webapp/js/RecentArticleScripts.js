$(document).ready(function () {
    loadArticles();
	showRecentArticle();
});

function loadArticles() {
	$.ajax({
		url: 'articlesByApproval'
	}).success(function (data, status) {
		fillArticleSearchTable(data, status);
	});
}

function showRecentArticle() {
    $.ajax({
        url: 'mostRecentArticle'
    }).success(function (data, status) {
        displaySearchedArticleFromSearch(data, status);
    });
}