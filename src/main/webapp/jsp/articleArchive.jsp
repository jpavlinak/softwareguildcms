<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
		
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Article Archive</title>

        <%@include file="commonLinksFragment.jsp" %>
        <link href="https://fonts.googleapis.com/css?family=Cinzel" rel="stylesheet">

    </head>
    <body>
		
        <div id="userTopNavbar" class="container-fluid">
			
            <%@include file="mainNavBarFragment.jsp" %>
			<%@include file="searchResultFragment.jsp" %>

        </div>
            
        <%@include file="commonCustomJSFragment.jsp" %>
		<script src="${pageContext.request.contextPath}/js/RecentArticleScripts.js"></script>
        
    </body>
</html>