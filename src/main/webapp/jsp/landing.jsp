<%-- 
    Document   : landing
    Created on : Dec 2, 2016, 3:48:12 PM
    Author     : apprentice
--%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Landing</title>
		
		<%@include file="commonLinksFragment.jsp" %>
        <link href="https://fonts.googleapis.com/css?family=Cinzel" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-offset-6 col-sm-8">
					<h1 id="title">The<br> Beer Blog</h1>
				</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-offset-6 col-sm-6">
					<strong>Are you at least 21 years old?</strong>
				</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-offset-6 col-sm-5">
					<a class="btn btn-default" href="${pageContext.request.contextPath}/home">Yes</a>
					<a class="btn btn-default" href="http://www.warnerbros.com/archive/spacejam/movie/jam.htm">No</a>
				</div>
			</div>
		</div>
		<%@include file="commonJSFragment.jsp" %>
	</body>
</html>
